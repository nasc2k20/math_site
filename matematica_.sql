/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : SERVIDOR:3306
 Source Schema         : matematica

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 12/09/2019 18:16:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area`  (
  `CodArea` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NombreArea` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`CodArea`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES (1, 'ALGEBRA');
INSERT INTO `area` VALUES (2, 'PROBABILIDADES');
INSERT INTO `area` VALUES (3, 'GEOMETRIA ANALITICA');
INSERT INTO `area` VALUES (4, 'GEOMETRIA');
INSERT INTO `area` VALUES (5, 'CALCULO');
INSERT INTO `area` VALUES (6, 'TRIGONOMETRIA');
INSERT INTO `area` VALUES (7, 'ALGEBRA LINEAL');
INSERT INTO `area` VALUES (8, 'ESTADISTICA');
INSERT INTO `area` VALUES (9, 'ARITMETICA');

-- ----------------------------
-- Table structure for ejercicio
-- ----------------------------
DROP TABLE IF EXISTS `ejercicio`;
CREATE TABLE `ejercicio`  (
  `CodEjercicio` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `DescripcionEjercicio` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `EnunciadoEjercicio` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CodArea` int(15) NOT NULL,
  `RespEjercicio` int(2) NOT NULL,
  PRIMARY KEY (`CodEjercicio`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ejercicio
-- ----------------------------
INSERT INTO `ejercicio` VALUES (1, 'UN ALPINISTA', 'Un alpinista observa una montaña que quiere escalar. \nÉl escribe en una imagen los datos que conoce y con una letra 𝐡 el valor desconocido de la altura.\nSi él aplica lo aprendido en Matemática, ¿qué valor obtuvo para 𝐡?', 4, 0);
INSERT INTO `ejercicio` VALUES (2, 'UN DOCTOR RECETA A SU PACIENTE', 'Un doctor receta a su paciente un jarabe para una tos persistente.\nLe indica que el primer día debe tomar 110 ml y debe disminuir la dosis en 5 ml cada día, respecto al día anterior, ¿cuántos ml habrá tomado durante 20 días del tratamiento?', 8, 0);
INSERT INTO `ejercicio` VALUES (3, 'JOSé TRABAJA COMO DEPENDIENTE EN UNA TIENDA', 'José trabaja como dependiente en una tienda, además de vender, tiene que empacar los productos a los  clientes  después  de  realizar  la  compra.  En  los  últimos  días  ha  tenido  algunas  discusiones  con los clientes, pues él empaqueta los artículos con el menor número de bolsas plásticas posibles. Los clientes \nse molestan y le reclaman acusándolo de tacaño y exigiéndole que ponga más bolsas al empaquetar la mercadería. José les explica la importancia de hacer prácticas ecológicas y lo mucho que ayudan al medio ambiente el reducir y reutilizar cosas.\n\nSegún el caso anterior, determina qué acción es la que más ayudaría al medio ambiente.', 5, 0);

-- ----------------------------
-- Table structure for respuesta
-- ----------------------------
DROP TABLE IF EXISTS `respuesta`;
CREATE TABLE `respuesta`  (
  `CodRespuesta` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `CodEjercicio` int(15) NOT NULL,
  `DesarrolloRespuesta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RespuestaRapida` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`CodRespuesta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 123 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of respuesta
-- ----------------------------
INSERT INTO `respuesta` VALUES (124, 3, '355467oferta-especial.png', 'asdfd');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `LoginUsuario` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ContrasenaUsuario` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NombreUsuario` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CorreoUsuario` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `EstadoUsuario` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`LoginUsuario`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('admin', 'YWRtaW4=', 'NAHUM', 'nasc', 'activo');

SET FOREIGN_KEY_CHECKS = 1;
