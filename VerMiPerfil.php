<?php
include 'login/ValidarSesion.php';
include 'conexion/Conectar.php';

$UsuarioLog = $_SESSION['LoginUsuario'];
$CodGrado = $_SESSION['CodGrado'];

$SelUsus = "SELECT * 
            FROM usuario a 
            INNER JOIN grado b ON b.CodGrado=a.CodGrado 
            WHERE a.LoginUsuario='" . $UsuarioLog . "' AND a.CodGrado='" . $CodGrado . "'";
$EjeUsus = $Cnn->prepare($SelUsus);
$EjeUsus->execute();
$VerUsus = $EjeUsus->fetch(PDO::FETCH_ASSOC);
$LoginUsuario = $VerUsus['LoginUsuario'];
$NombreUsuario = $VerUsus['NombreUsuario'];
$CorreoUsuario = $VerUsus['CorreoUsuario'];
$EstadoUsuario = $VerUsus['EstadoUsuario'];
$NombreGrado = $VerUsus['NombreGrado'];
?>
<style>
    .container {
        padding: 5%;
    }

    .container .img {
        text-align: center;
    }

    .container .details {
        border-left: 3px solid #ded4da;
    }

    .container .details p {
        font-size: 15px;
        font-weight: bold;
    }
</style>
<div class="container">
    <span>
        <h3 style="text-align:center;"><?php echo $NombreGrado; ?></h3>
    </span>
    <div style="height: 1rem;"></div>
    <div class="row">
        <div class="col-md-6 img">
            <img src="../images/user_img.png" alt="" class="img-rounded">
        </div>
        <div class="col-md-6 details">
            <blockquote>
                <h5>NOMBRE COMPLETO: <br> <?php echo $NombreUsuario; ?> 
                <?php echo ($EstadoUsuario=='activo') ? '<i class="fas fa-user-check" style="color: #1EC4ED;"></i>' : '<i class="fas fa-user-times" style="color: red;"></i>' ;?>
            </h5>
                <small><cite title="Source Title">CORREO: <?php echo $CorreoUsuario; ?> <i class="icon-map-marker"></i></cite></small>
            </blockquote>
            <p> USUARIO:
                <?php echo $LoginUsuario; ?> <br>
            </p>
        </div>
    </div>
</div>