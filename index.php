<!DOCTYPE html>
<html lang="en">

<head>
	<title>MathInfo</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="login/images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/css/util.css">
	<link rel="stylesheet" type="text/css" href="login/css/main.css">
	<!--===============================================================================================-->
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="login/images/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="login/ProcesoSQL.php" method="POST">
					<span class="login100-form-title">
						Inicio de Sesi&oacute;n
					</span>

					<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" id="TxtUsuario" name="TxtUsuario" placeholder="Usuario">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" id="TxtContrasena" name="TxtContrasena" placeholder="Contrase&ntilde;a">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="Button1">
							Ingresar
						</button>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#" data-toggle="modal" data-target="#RegistrarUsuarioModal">
							Crear Tu Cuenta
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="RegistrarUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="RegistrarUsuarioModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="RegistrarUsuarioModalTitle">Registrar Usuario</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-6">
							<label for="LoginUsuTxt">Usuario: </label>
							<input type="text" class="form-control" id="LoginUsuTxt" placeholder="Nombre de Usuario">
						</div>
						<div class="col-6">
							<label for="ContrasenaUsuTxt">Contrase&ntilde;a: </label>
							<input type="password" class="form-control" id="ContrasenaUsuTxt" placeholder="Contrase&ntilde;a de Usuario">
						</div>
					</div>

					<div style="height: 2rem;"></div>

					<div class="row">
						<div class="col-7">
							<label for="NombreUsuTxt">Nombre Completo: </label>
							<input type="text" class="form-control" id="NombreUsuTxt" placeholder="Nombre Completo" style="text-transform:uppercase;">
						</div>
						<div class="col-5">
							<label for="CorreoUsuTxt">Correo: </label>
							<input type="text" class="form-control" id="CorreoUsuTxt" placeholder="@">
						</div>
					</div>

					<div style="height: 2rem;"></div>

					<div class="row">
						<div class="col">
							<label for="GradoSltA">Seleccione su Bachillerato: </label>
							<select id="GradoSltA" class="form-control btn-secondary"></select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cerrar</button>
					<button type="button" class="btn btn-success" onclick="VerificarUsuario();" id="BtnAgregarUsu"><i class="fa fa-floppy-o"></i> Guardar</button>
				</div>
			</div>
		</div>
	</div>



	<!--===============================================================================================-->
	<script src="login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/bootstrap/js/popper.js"></script>
	<script src="login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/tilt/tilt.jquery.min.js"></script>
	<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<!--===============================================================================================-->
	<script src="login/js/main.js"></script>
	<script src="modulos.js"></script>

</body>

</html>