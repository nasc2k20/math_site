$(document).ready(function(){
  $.post("modulos/usuarios/LlenarComboGrado.php", function(datos){
      $('#GradoSltA').html(datos).fadein('slow');
  });
});

function VerificarUsuario() {
    var UsuarioTxt = $('#LoginUsuTxt').val();
    var CorreoUsuTxt = $('#CorreoUsuTxt').val();
    var NombreUsuTxt = $('#NombreUsuTxt').val();
    var ContrasenaUsuTxt = $('#ContrasenaUsuTxt').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "CorreoUsuTxt": CorreoUsuTxt,
        "NombreUsuTxt": NombreUsuTxt,
        "ContrasenaUsuTxt": ContrasenaUsuTxt,
        "UsuarioTxt": UsuarioTxt,
        "SqlQuery": SqlQuery
    };

    $.ajax({
        method: 'POST',
        url: 'VerificarNuevoUsuario.php',
        data: Parametros,
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (datos) {
            console.log(datos.Mensaje);

            if (datos.Mensaje == 'NoExiste') {
                AgregarUsuario();
            } else if (datos.Mensaje = 'Existe') {
                alert('El Usuario Ya Existe en la Base de Datos, Elija Otro Usuario');
            } else {

            }
        }
    });
}

function AgregarUsuario() {
    var UsuarioTxt = $('#LoginUsuTxt').val();
    var CorreoUsuTxt = $('#CorreoUsuTxt').val();
    var NombreUsuTxt = $('#NombreUsuTxt').val();
    var ContrasenaUsuTxt = $('#ContrasenaUsuTxt').val();
    var CodGrado = $('#GradoSltA option:selected').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "CorreoUsuTxt": CorreoUsuTxt,
        "NombreUsuTxt": NombreUsuTxt,
        "ContrasenaUsuTxt": ContrasenaUsuTxt,
        "UsuarioTxt": UsuarioTxt,
        "CodGrado": CodGrado,
        "SqlQuery": SqlQuery
    };

    if (UsuarioTxt == '') {
        alert('Debe de Digitar el Usuario');
        $('#LoginUsuTxt').focus();
    } else if (CorreoUsuTxt == '') {
        alert('Debe de Digitar el Correo');
        $('#CorreoUsuTxt').focus();
    } else if (NombreUsuTxt == '') {
        alert('Debe de Digitar el Nombre del Usuario');
        $('#NombreUsuTxt').focus();
    } else if (ContrasenaUsuTxt == '') {
        alert('Debe de Digitar La Contraseña');
        $('#ContrasenaUsuTxt').focus();
    } else if (CodGrado == '') {
        alert('Debe de seleccionar un Grado');
        $('#GradoSltA').focus();
    } else {
        $.ajax({
            method: 'POST',
            url: 'AgregarNuevoUsuario.php',
            data: Parametros,
            beforeSend: function () {
                $("#MostrarDatosUsuario").html("<center><img src='images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datosAdd) {
                location.href = 'index.php';
            }
        });

    }

    event.preventDefault();
}