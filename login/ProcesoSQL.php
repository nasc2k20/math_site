<?php
if (isset($_POST["Button1"]))
{
	$usu = $_POST["TxtUsuario"];
	$pas = base64_encode($_POST["TxtContrasena"]);
	$est = "activo";
	
	require_once '../conexion/Conectar.php';
	
	$sel_usu = "SELECT * FROM usuario 
				WHERE LoginUsuario=:LoginUsuario AND ContrasenaUsuario=:ContrasenaUsuario 
				AND EstadoUsuario=:EstadoUsuario";
	$eje_usu = $Cnn->prepare($sel_usu);
	$eje_usu->bindParam(":LoginUsuario",$usu,PDO::PARAM_STR);
	$eje_usu->bindParam(":ContrasenaUsuario",$pas,PDO::PARAM_STR);
	$eje_usu->bindParam(":EstadoUsuario",$est,PDO::PARAM_STR);
	$eje_usu->execute();
	$con = $eje_usu->rowCount();
	
	if($con > 0)
	{
		$dat = $eje_usu->fetch(PDO::FETCH_ASSOC);
		session_start();
		header("location: ../modulos/index.php");
		$_SESSION["NombreUsuario"] = $dat["NombreUsuario"];
		$_SESSION["UsuarioNivel"] = $dat["UsuarioNivel"];
		$_SESSION["LoginUsuario"] = $dat["LoginUsuario"];
		$_SESSION["CodGrado"] = $dat["CodGrado"];
		$_SESSION["practicas"] = "InciarSesion";
	}
	else
	{
		?>
        <script language="javascript">
		location.href = '../index.php'
		alert('Usuario No Registrado')
        </script>
        <?php
	}
	
}
else
{
	echo "No hay Datos";
}

?>
