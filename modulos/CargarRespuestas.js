$(function () {
    CargarProcesos(1);
});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var CodeArea = ($('#AreaSltR option:selected').val() > 0 ? $('#AreaSltR option:selected').val() : 'MostrarTodos');

    var Datos = {
        "CodeArea": CodeArea,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: "respuestas/VerRespuestas.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosRespuestas").html("<center><img src='images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosRespuestas').html(data).fadeIn('slow');
        }
    });
}