<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

if ($_POST['SqlQuery'] == 'insertar') {
	try {
		$NombreArea		= ($_POST["NombreArea"] == '' ? ' ' : strtoupper($_POST["NombreArea"]));


		$InserArea = "INSERT INTO area (NombreArea)
		VALUES(:NombreArea)";
		$InserArea = $Cnn->prepare($InserArea);
		$InserArea->bindparam(":NombreArea", $NombreArea, PDO::PARAM_STR);

		if ($InserArea->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Agregar los Datos:\n";
			print_r($InserArea->errorInfo());
			echo "<br>El Codigo de Error es: " . $InserArea->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL AGREGAR DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL AGREGAR DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL AGREGAR DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'actualizar') {
	try {
		$CodAreaTxtE		= $_POST["CodAreaTxtE"];
		$NombreArea		= ($_POST["NombreAreaTxtE"] == '' ? ' ' : strtoupper($_POST["NombreAreaTxtE"]));


		$UpdaArea = "UPDATE area 
						SET 
						NombreArea=:NombreArea 
						WHERE CodArea=:CodArea";
		$UpdaArea = $Cnn->prepare($UpdaArea);
		$UpdaArea->bindparam(":CodArea", $CodAreaTxtE, PDO::PARAM_STR);
		$UpdaArea->bindparam(":NombreArea", $NombreArea, PDO::PARAM_STR);

		if ($UpdaArea->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Actualizar los Datos:\n";
			print_r($UpdaArea->errorInfo());
			echo "<br>El Codigo de Error es: " . $UpdaArea->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ACTUALIZAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'eliminar') {

	try {
		$CodAreaTxtE     = $_POST['CodeArea'];

		$DeleArea = "DELETE FROM area WHERE CodArea=:CodArea";
		$DeleArea = $Cnn->prepare($DeleArea);
		$DeleArea->bindParam(":CodArea", $CodAreaTxtE, PDO::PARAM_STR);


		if ($DeleArea->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Eliminar los Datos:\n";
			print_r($DeleArea->errorInfo());
			echo "<br>El Codigo de Error es: " . $DeleArea->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ELIMINAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ELIMINAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ELIMINAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} else { }
