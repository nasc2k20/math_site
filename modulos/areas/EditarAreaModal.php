<div class="modal fade" id="EditarAreaModal" tabindex="-1" role="dialog" aria-labelledby="EditarAreaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EditarAreaModalLabel">Modificar Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" id="CodAreaTxtE">
                    <label for="NombreAreaTxtE">Nombre Area: </label>
                    <input type="text" class="form-control" id="NombreAreaTxtE" placeholder="Nombre del Area" style="text-transform:uppercase;">
                </div>                                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="EditarArea();" id="BtnEditarArea"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>