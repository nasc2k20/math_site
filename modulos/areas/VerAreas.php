<?php
require_once '../../conexion/Conectar.php';
include_once '../../conexion/Paginator.php';

$BusquedaArea = $_REQUEST['BusquedaArea'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if ($BusquedaArea != '') {
  $QueryPag   = "SELECT count(*) as numrows 
                    FROM area 
                    WHERE NombreArea LIKE '%" . $BusquedaArea . "%' 
                    ORDER BY NombreArea ASC";
} else {
  $QueryPag   = "SELECT count(*) AS numrows
                    FROM area";
}

$eje_area = $Cnn->prepare($QueryPag);
$eje_area->execute();
$dat = $eje_area->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag'])) ? $_REQUEST['NumPag'] : 1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows / $per_page);

if ($BusquedaArea != '') {
  $sel_area = "SELECT * FROM area
                WHERE NombreArea LIKE '%" . $BusquedaArea . "%' 
                ORDER BY NombreArea ASC 
                LIMIT $offset, $per_page";
} else {
  $sel_area = "SELECT * FROM area ORDER BY NombreArea ASC  
                LIMIT $offset, $per_page";
}


$eje_area = $Cnn->prepare($sel_area);
$eje_area->execute();
?>
<table class="table table-bordered table-striped table-hover table-sm">
  <thead class="thead-dark">
    <tr>
      <th class="center">No.</th>
      <th class="center">Nombre de Area</th>
      <th class="center" colspan="2">Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $finales = 0;
    while ($ver_area = $eje_area->fetch(PDO::FETCH_ASSOC)) {
      $NombreArea = $ver_area['NombreArea'];
      $CodArea = $ver_area['CodArea'];
      ?>
      <tr>
        <td width='4%' class='center'><?php echo $ContarNum; ?></td>
        <td width='90%'><?php echo $NombreArea; ?></td>
        <td class='center' width='3%' data-toggle='tooltip' data-placement='top' title='Modificar'>
        <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#EditarAreaModal" data-nombrearea="<?php echo $NombreArea; ?>" data-codarea="<?php echo $CodArea; ?>"><i class="fas fa-pen-square" aria-hidden="true"></i> </button>
        </td>
        <td class='center' width='3%' data-toggle="tooltip" data-placement="top" title="Eliminar">
          <button class="btn btn-outline-danger btn-sm" onclick="EliminarArea('<?php echo $CodArea; ?>','<?php echo $NombreArea; ?>')"><i class="fas fa-trash" aria-hidden="true" title="Eliminar"></i></button>
        </td>
      </tr>
      <?php
      $finales++;
      $ContarNum++;
    }
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">
        <?php
        $inicios = $offset + 1;
        $finales += $inicios - 1;
        echo "Mostrando $inicios al $finales de $numrows registros";
        echo paginate($page, $total_pages, $adjacents);
        ?>
      </td>
    </tr>
  </tfoot>
</table>