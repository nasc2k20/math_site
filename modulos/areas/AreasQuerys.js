var DirectorioArea = 'areas/';

$(function () {
    $('#AgregarAreaModal').on('shown.bs.modal', function () {
        $('#NombreAreaTxt').focus();
    });

    CargarProcesos(1);

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

    $('#EditarAreaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var NombreArea = button.data('nombrearea');
        $('#NombreAreaTxtE').val(NombreArea);
        var CodArea = button.data('codarea');
        $('#CodAreaTxtE').val(CodArea);
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var BusquedaArea = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaArea": BusquedaArea,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioArea + "VerAreas.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosArea").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosArea').html(data).fadeIn('slow');
        }
    });
}

function AgregarArea() {
    var NombreArea = $('#NombreAreaTxt').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "NombreArea": NombreArea
    };

    if (NombreArea == '') {
        alert('Debe de Digitar el Nombre del Area');
        $('#NombreAreaTxt').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioArea + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosArea").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosArea").html(datos);
                CargarProcesos(1);
                $('#AgregarAreaModal').modal('hide');
                /*
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                */

            }
        });
    }

    event.preventDefault();
}


function EditarArea() {
    var NombreAreaTxtE = $('#NombreAreaTxtE').val();
    var CodAreaTxtE = $('#CodAreaTxtE').val();
    var SqlQuery = 'actualizar';

    var Datos = {
        "NombreAreaTxtE": NombreAreaTxtE,
        "CodAreaTxtE": CodAreaTxtE,
        "SqlQuery": SqlQuery
    };

    if (NombreAreaTxtE == '') {
        alert('Debe de Digitar el Nombre del Area');
        $('#NombreAreaTxtE').focus();
    } else if (CodAreaTxtE <=0) {
        alert('El codigo del area es Invalido');
        $('#NombreAreaTxtE').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioArea + "ProcesosSQL.php",
            data: Datos,
            beforeSend: function (objeto) {
                $("#MostrarDatosArea").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosArea").html(datos);
                CargarProcesos(1);
                $('#EditarAreaModal').modal('hide');
            }
        });
    }


    event.preventDefault();
}

function EliminarArea(CodeArea, NombreArea) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "CodeArea": CodeArea
    }

    $.confirm({
        icon: 'fas fa-smile',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Area: <br> <strong><h3>' + NombreArea + '</h3></strong>',
        animation: 'scale',
        icon: 'fas fa-exclamation-triangle',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioArea + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosArea").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosArea").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}