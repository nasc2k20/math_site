<?php
require_once '../../conexion/Conectar.php';

$SelArea = "SELECT * 
              FROM area 
              ORDER BY NombreArea ASC";
$EjeArea = $Cnn->prepare($SelArea);
$EjeArea->execute();

echo '<option value="MostrarTodos">MOSTRAR TODOS</option>';
while ($VerArea = $EjeArea->fetch(PDO::FETCH_ASSOC)) {
    echo '<option value="' . $VerArea['CodArea'] . '">' . $VerArea['NombreArea'] . '</option>';
}