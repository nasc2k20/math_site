<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

if ($_POST['SqlQuery'] == 'insertar') {
	try {
		$DescripcionEjercTxt	= ($_POST["DescripcionEjercTxt"] == '' ? ' ' : strtoupper($_POST["DescripcionEjercTxt"]));
		$EnunciadoEjercTxt		= ($_POST["EnunciadoEjercTxt"] == '' ? ' ' : $_POST["EnunciadoEjercTxt"]);
		$AreasEjercSlt			= $_POST["AreasEjercSlt"];
        $RespEjercicio = 0;

		$InserEjerc = "INSERT INTO ejercicio (DescripcionEjercicio, EnunciadoEjercicio, CodArea, RespEjercicio)
						VALUES(:DescripcionEjercicio, :EnunciadoEjercicio, :CodArea, :RespEjercicio)";
		$InserEjerc = $Cnn->prepare($InserEjerc);
		$InserEjerc->bindparam(":DescripcionEjercicio", $DescripcionEjercTxt, PDO::PARAM_STR);
		$InserEjerc->bindparam(":EnunciadoEjercicio", $EnunciadoEjercTxt, PDO::PARAM_STR);
		$InserEjerc->bindparam(":CodArea", $AreasEjercSlt, PDO::PARAM_STR);
		$InserEjerc->bindparam(":RespEjercicio", $RespEjercicio, PDO::PARAM_STR);

		if ($InserEjerc->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Agregar los Datos:\n";
			print_r($InserEjerc->errorInfo());
			echo "<br>El Codigo de Error es: " . $InserEjerc->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL AGREGAR DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL AGREGAR DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL AGREGAR DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'actualizar') {
	try {
		$DescripcionEjercTxtE	= ($_POST["DescripcionEjercTxtE"] == '' ? ' ' : strtoupper($_POST["DescripcionEjercTxtE"]));
		$EnunciadoEjercTxtE		= ($_POST["EnunciadoEjercTxtE"] == '' ? ' ' : $_POST["EnunciadoEjercTxtE"]);
		$AreasEjercSltE			= $_POST["AreasEjercSltE"];
		$CodEjercicioTxtE		= $_POST["CodEjercicioTxtE"];


		$UpdaEjerc = "UPDATE ejercicio  
						SET 
						DescripcionEjercicio=:DescripcionEjercicio, 
						EnunciadoEjercicio=:EnunciadoEjercicio, 
						CodArea=:CodArea 
						WHERE CodEjercicio=:CodEjercicio";
		$UpdaEjerc = $Cnn->prepare($UpdaEjerc);
		$UpdaEjerc->bindparam(":CodEjercicio", $CodEjercicioTxtE, PDO::PARAM_STR);
		$UpdaEjerc->bindparam(":DescripcionEjercicio", $DescripcionEjercTxtE, PDO::PARAM_STR);
		$UpdaEjerc->bindparam(":EnunciadoEjercicio", $EnunciadoEjercTxtE, PDO::PARAM_STR);
		$UpdaEjerc->bindparam(":CodArea", $AreasEjercSltE, PDO::PARAM_STR);

		if ($UpdaEjerc->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Actualizar los Datos:\n";
			print_r($UpdaEjerc->errorInfo());
			echo "<br>El Codigo de Error es: " . $UpdaEjerc->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ACTUALIZAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'eliminar') {

	try {
		$CodeEjercicio     = $_POST['CodeEjercicio'];

		$DeleEjerc = "DELETE FROM ejercicio WHERE CodEjercicio=:CodEjercicio";
		$DeleEjerc = $Cnn->prepare($DeleEjerc);
		$DeleEjerc->bindParam(":CodEjercicio", $CodeEjercicio, PDO::PARAM_STR);


		if ($DeleEjerc->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Eliminar los Datos:\n";
			print_r($DeleEjerc->errorInfo());
			echo "<br>El Codigo de Error es: " . $DeleEjerc->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ELIMINAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ELIMINAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ELIMINAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
}
