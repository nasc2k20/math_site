var DirectorioEjerc = 'respuestas/';

$(function () {
    $.post(DirectorioEjerc + "CargarAreaComboBuscarR.php", function (data) {
        $("#AreaSltR").html(data);
    });

    CargarProcesos(1);

    $('#AreaSltR').change(function () {
        var id = $(this).find(":selected").val();
        CargarProcesos(1);
    });

    $('#EditarEjercModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var DespcricionEjercTxt = button.data('descripcionejercicio');
        $('#DescripcionEjercTxtE').val(DespcricionEjercTxt);
        var CodArea = button.data('codearea');
        $('#AreasEjercSltE').val(CodArea);
        //var NombreArea = button.data('nombrearea');
        //$('#AreasEjercSltE').text(NombreArea);
        var EnunciadoEjerc = button.data('enunciadoejercicio');
        $('#EnunciadoEjercTxtE').val(EnunciadoEjerc);
        var CodEjercicio = button.data('codeejercicio');
        $('#CodEjercicioTxtE').val(CodEjercicio);
    });
    
    $('#MostrarImagenModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var NombreImg = button.data('nombreimagen');
        var Ruta = '../../../imgresp/';
        $('#MostrarImagenDiv').attr('src', Ruta + NombreImg);
    });
    
});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var CodeArea = ($('#AreaSltR option:selected').val() > 0 ? $('#AreaSltR option:selected').val() : 'MostrarTodos');

    var Datos = {
        "CodeArea": CodeArea,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioEjerc + "VerRespuestas.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosRespuestas").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosRespuestas').html(data).fadeIn('slow');
        }
    });
}