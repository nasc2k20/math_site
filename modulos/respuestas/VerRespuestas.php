<?php
include '../../login/ValidarSesion.php';
require_once '../../conexion/Conectar.php';
include_once '../../conexion/Paginator.php';

$CodGrado = $_SESSION["CodGrado"];
$CodeArea = $_REQUEST['CodeArea'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if ($CodeArea == 'MostrarTodos') {
    $QueryPag   = "SELECT count(*) AS numrows
                    FROM ejercicio
                    WHERE CodGrado='" . $CodGrado . "'";
} else {
    $QueryPag   = "SELECT count(*) as numrows 
                    FROM ejercicio 
                    WHERE CodArea = '" . $CodeArea . "' AND CodGrado='" . $CodGrado . "'";
}

$eje_ejerci = $Cnn->prepare($QueryPag);
$eje_ejerci->execute();
$dat = $eje_ejerci->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag'])) ? $_REQUEST['NumPag'] : 1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows / $per_page);


if ($CodeArea == 'MostrarTodos') {
    $sel_ejerc = "SELECT * 
                FROM ejercicio a 
                INNER JOIN area b ON b.CodArea=a.CodArea 
                WHERE a.CodGrado = '" . $CodGrado . "' 
                ORDER BY a.DescripcionEjercicio ASC  
                LIMIT $offset, $per_page";
} else {
    $sel_ejerc = "SELECT * FROM ejercicio a   
                INNER JOIN area b ON b.CodArea=a.CodArea 
                WHERE a.CodArea = '" . $CodeArea . "' AND a.CodGrado='" . $CodGrado . "'
                ORDER BY a.DescripcionEjercicio ASC 
                LIMIT $offset, $per_page";
}
$eje_ejerc = $Cnn->prepare($sel_ejerc);
$eje_ejerc->execute();
?>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover table-sm">
        <thead class="thead-dark">
            <tr>
                <th class="center">No.</th>
                <th class="center">Descripcion del Ejercicio</th>
                <th class="center">Enunciado del Ejercicio</th>
                <th class="center">Area</th>
                <th class="center">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $finales = 0;
            while ($ver_ejerc = $eje_ejerc->fetch(PDO::FETCH_ASSOC)) {
                $NombreArea = $ver_ejerc['NombreArea'];
                $CodArea = $ver_ejerc['CodArea'];
                $DescripcionEjercicio = $ver_ejerc['DescripcionEjercicio'];
                $EnunciadoEjercicio = $ver_ejerc['EnunciadoEjercicio'];
                $CodEjercicio = $ver_ejerc['CodEjercicio'];
                $RespEjercicio = $ver_ejerc['RespEjercicio'];

                $SelResImg   = "SELECT * FROM respuesta 
                    WHERE CodEjercicio = '" . $CodEjercicio . "'";
                $EjeResImg = $Cnn->prepare($SelResImg);
                $EjeResImg->execute();
                $ConResImg = $EjeResImg->rowCount();

                if ($ConResImg > 0) {
                    $VerResImg = $EjeResImg->fetch(PDO::FETCH_ASSOC);
                    $NombreImg = $VerResImg['DesarrolloRespuesta'];
                } else {
                    $NombreImg = '';
                }
                ?>
                <tr>
                    <td width='4%' class='center'><?php echo $ContarNum; ?></td>
                    <td width='20%'><?php echo $DescripcionEjercicio; ?></td>
                    <td width='50%'><?php echo $EnunciadoEjercicio; ?></td>
                    <td width='20%'><?php echo $NombreArea; ?></td>
                    <td width='3%' data-toggle='tooltip' data-placement='top' title='Visualizar Imagen'>
                        <?php if ($RespEjercicio > 0) { ?>
                            <button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#MostrarImagenModal" data-nombreimagen="<?php echo $NombreImg; ?>"><i class="fas fa-search" aria-hidden="true"></i> </button>
                        <?php } else { ?>
                            <button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#MostrarImagenModal" disabled><i class="fas fa-search" aria-hidden="true"></i> </button>
                        <?php } ?>
                    </td>
                </tr>
            <?php
                $finales++;
                $ContarNum++;
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php
                    $inicios = $offset + 1;
                    $finales += $inicios - 1;
                    echo "Mostrando $inicios al $finales de $numrows registros";
                    echo paginate($page, $total_pages, $adjacents);
                    ?>
                </td>
            </tr>
        </tfoot>
    </table>
</div>