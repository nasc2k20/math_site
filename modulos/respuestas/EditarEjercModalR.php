<div class="modal fade" id="EditarEjercModal" tabindex="-1" role="dialog" aria-labelledby="EditarEjercModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EditarEjercModalLabel">Modificar Ejercicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" id="CodEjercicioTxtE">
                    <label for="DescripcionEjercTxtE">Descripci&oacute;n: </label>
                    <input type="text" class="form-control" id="DescripcionEjercTxtE" placeholder="Tema del Ejercicio" style="text-transform:uppercase;">
                </div>
                <div class="form-group">
                    <label for="AreasEjercSltE">Area: </label>
                    <select id="AreasEjercSltE" class="form-control btn-secondary"></select>
                </div>
                <div class="form-group">
                    <label for="EnunciadoEjercTxtE">Enunciado: </label>
                    <textarea id="EnunciadoEjercTxtE" class="form-control" cols="30" rows="10" style="resize:none;" placeholder="Enunciado"></textarea>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="EditarEjercicio();" id="BtnEditarEjerc"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>