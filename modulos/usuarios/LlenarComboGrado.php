<?php
require_once '../../conexion/Conectar.php';

$eje_grado = "SELECT * 
              FROM grado 
              ORDER BY NombreGrado ASC";
$eje_grado = $Cnn->prepare($eje_grado);
$eje_grado->execute();

while ($ver_grado = $eje_grado->fetch(PDO::FETCH_ASSOC)) {
    echo '<option value="' . $ver_grado['CodGrado'] . '">' . $ver_grado['NombreGrado'] . '</option>';
}
