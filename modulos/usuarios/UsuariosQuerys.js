var DirectorioUsu = 'usuarios/';

$(function () {
    $('#AgregarUsuModal').on('shown.bs.modal', function () {
        $('#LoginUsuTxt').focus();
    });

    CargarProcesos(1);

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

    $('#EditarUsuModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var LoginUsuario = button.data('loginusuario');
        $('#LoginUsuTxtE').val(LoginUsuario);
        var ContrasenaUsuario = button.data('contrasusuario');
        $('#ContrasenaUsuTxtE').val(ContrasenaUsuario);
        var NombreUsuario = button.data('nombreusuario');
        $('#NombreUsuTxtE').val(NombreUsuario);
        var CorreoUsuario = button.data('correousuario');
        $('#CorreoUsuTxtE').val(CorreoUsuario);
        //var NombreGrado = button.data('nombregrado');
        //$('#GradoSltE').text(NombreGrado);
        var CodeGrado = button.data('codgrado');
        $('#GradoSltE').val(CodeGrado);
    });

    $.post(DirectorioUsu + "LlenarComboGrado.php", function (datas) {
        $('#GradoSlt').html(datas).fadeIn('slow');
    });

    $.post(DirectorioUsu + "LlenarComboGrado.php", function (datas) {
        $('#GradoSltE').html(datas).fadeIn('slow');
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var BusquedaUsu = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaUsu": BusquedaUsu,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioUsu + "VerUsuarios.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosUsuario").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosUsuario').html(data).fadeIn('slow');
        }
    });
}

function VerificarUsuario() {
    var UsuarioTxt = $('#LoginUsuTxt').val();
    var CorreoUsuTxt = $('#CorreoUsuTxt').val();
    var NombreUsuTxt = $('#NombreUsuTxt').val();
    var ContrasenaUsuTxt = $('#ContrasenaUsuTxt').val();
    var GradoSlt = $('#GradoSlt option:selected').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "CorreoUsuTxt": CorreoUsuTxt,
        "NombreUsuTxt": NombreUsuTxt,
        "ContrasenaUsuTxt": ContrasenaUsuTxt,
        "UsuarioTxt": UsuarioTxt,
        "GradoSlt": GradoSlt,
        "SqlQuery": SqlQuery
    };

    $.ajax({
        method: 'POST',
        url: DirectorioUsu + 'VerificarUsuario.php',
        data: Parametros,
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (datos) {
            console.log(datos.Mensaje);

            if (datos.Mensaje == 'NoExiste') {
                AgregarUsuario();
            } else if (datos.Mensaje = 'Existe') {
                alert('El Usuario Ya Existe en la Base de Datos, Comuniquese con el Administrador');
            } else {

            }
        }
    });
}

function AgregarUsuario() {
    var UsuarioTxt = $('#LoginUsuTxt').val();
    var CorreoUsuTxt = $('#CorreoUsuTxt').val();
    var NombreUsuTxt = $('#NombreUsuTxt').val();
    var ContrasenaUsuTxt = $('#ContrasenaUsuTxt').val();
    var GradoSlt = $('#GradoSlt option:selected').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "CorreoUsuTxt": CorreoUsuTxt,
        "NombreUsuTxt": NombreUsuTxt,
        "ContrasenaUsuTxt": ContrasenaUsuTxt,
        "UsuarioTxt": UsuarioTxt,
        "GradoSlt": GradoSlt,
        "SqlQuery": SqlQuery
    };

    if (UsuarioTxt == '') {
        alert('Debe de Digitar el Usuario');
        $('#LoginUsuTxt').focus();
    } else if (CorreoUsuTxt == '') {
        alert('Debe de Digitar el Correo');
        $('#CorreoUsuTxt').focus();
    } else if (NombreUsuTxt == '') {
        alert('Debe de Digitar el Nombre del Usuario');
        $('#NombreUsuTxt').focus();
    } else if (ContrasenaUsuTxt == '') {
        alert('Debe de Digitar La Contraseña');
        $('#ContrasenaUsuTxt').focus();
    } else if (GradoSlt == '') {
        alert('Debe de Seleccionar un Grado');
        $('#GradoSlt').focus();
    } else {
        $.ajax({
            method: 'POST',
            url: DirectorioUsu + 'ProcesosSQL.php',
            data: Parametros,
            beforeSend: function () {
                $("#MostrarDatosUsuario").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datosAdd) {
                $('#MostrarDatosUsuario').html(datosAdd).fadeIn('slow');
                CargarProcesos(1);
                $('#AgregarUsuModal').modal('hide');
            }
        });

    }

    event.preventDefault();
}


function EditarUsuario() {
    var UsuarioTxt = $('#LoginUsuTxtE').val();
    var CorreoUsuTxt = $('#CorreoUsuTxtE').val();
    var NombreUsuTxt = $('#NombreUsuTxtE').val();
    var ContrasenaUsuTxt = $('#ContrasenaUsuTxtE').val();
    var GradoSltE = $('#GradoSltE option:selected').val();
    var SqlQuery = 'actualizar';

    var Parametros = {
        "CorreoUsuTxt": CorreoUsuTxt,
        "NombreUsuTxt": NombreUsuTxt,
        "ContrasenaUsuTxt": ContrasenaUsuTxt,
        "UsuarioTxt": UsuarioTxt,
        "GradoSltE": GradoSltE,
        "SqlQuery": SqlQuery
    };

    if (UsuarioTxt == '') {
        alert('Debe de Digitar el Usuario');
        $('#LoginUsuTxtE').focus();
    } else if (CorreoUsuTxt == '') {
        alert('Debe de Digitar el Correo');
        $('#CorreoUsuTxtE').focus();
    } else if (NombreUsuTxt == '') {
        alert('Debe de Digitar el Nombre del Usuario');
        $('#NombreUsuTxtE').focus();
    } else if (ContrasenaUsuTxt == '') {
        alert('Debe de Digitar La Contraseña');
        $('#ContrasenaUsuTxtE').focus();
    } else if (GradoSltE == '') {
        alert('Debe de Seleccionar un Grado');
        $('#GradoSltE').focus();
    } else {
        $.ajax({
            method: 'POST',
            url: DirectorioUsu + 'ProcesosSQL.php',
            data: Parametros,
            beforeSend: function () {
                $("#MostrarDatosUsuario").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datosAdd) {
                $('#MostrarDatosUsuario').html(datosAdd).fadeIn('slow');
                CargarProcesos(1);
                $('#EditarUsuModal').modal('hide');
            }
        });

    }

    event.preventDefault();
}

function EliminarUsuario(LoginUsuario, NombreUsuario) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "UsuarioTxt": LoginUsuario
    }

    $.confirm({
        icon: 'fas fa-smile',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Usuario: <br> <strong><h3>' + LoginUsuario + ': ' + NombreUsuario + '</h3></strong>',
        animation: 'scale',
        icon: 'fas fa-exclamation-triangle',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioUsu + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosUsuario").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosUsuario").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}