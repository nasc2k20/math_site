<div class="modal fade" id="AgregarUsuModal" tabindex="-1" role="dialog" aria-labelledby="AgregarUsuModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AgregarUsuModalLabel">Nuevo Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <label for="LoginUsuTxt">Usuario: </label>
                        <input type="text" class="form-control" id="LoginUsuTxt" placeholder="Nombre de Usuario">
                    </div>
                    <div class="col-6">
                        <label for="ContrasenaUsuTxt">Contrase&ntilde;a: </label>
                        <input type="password" class="form-control" id="ContrasenaUsuTxt" placeholder="Contrase&ntilde;a de Usuario">
                    </div>
                </div>
                
                <div style="height: 2rem;"></div>
                
                <div class="row">
                    <div class="col-7">
                        <label for="NombreUsuTxt">Nombre Completo: </label>
                        <input type="text" class="form-control" id="NombreUsuTxt" placeholder="Nombre Completo" style="text-transform:uppercase;">
                    </div>
                    <div class="col-5">
                        <label for="CorreoUsuTxt">Correo: </label>
                        <input type="text" class="form-control" id="CorreoUsuTxt" placeholder="@">
                    </div>
                </div>
                
                <div style="height: 2rem;"></div>

                <div class="row">
                    <div class="col">
                        <label for="GradoSlt">Seleccione su Bachillerato: </label>
                        <select id="GradoSlt" class="form-control btn-secondary"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="VerificarUsuario();" id="BtnAgregarUsu"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
