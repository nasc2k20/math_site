<div class="modal fade" id="EditarUsuModal" tabindex="-1" role="dialog" aria-labelledby="EditarUsuModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EditarUsuModalLabel">Modificar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <label for="LoginUsuTxtE">Usuario: </label>
                        <input type="text" class="form-control" id="LoginUsuTxtE" placeholder="Nombre de Usuario" readonly>
                    </div>
                    <div class="col-6">
                        <label for="ContrasenaUsuTxtE">Contrase&ntilde;a: </label>
                        <input type="password" class="form-control" id="ContrasenaUsuTxtE" placeholder="Contrase&ntilde;a de Usuario">
                    </div>
                </div>
                
                <div style="height: 2rem;"></div>
                
                <div class="row">
                    <div class="col-7">
                        <label for="NombreUsuTxtE">Nombre Completo: </label>
                        <input type="text" class="form-control" id="NombreUsuTxtE" placeholder="Nombre Completo" style="text-transform:uppercase;">
                    </div>
                    <div class="col-5">
                        <label for="CorreoUsuTxtE">Correo: </label>
                        <input type="text" class="form-control" id="CorreoUsuTxtE" placeholder="@">
                    </div>
                </div>

                <div style="height: 2rem;"></div>

                <div class="row">
                    <div class="col">
                        <label for="GradoSltE">Seleccione su Bachillerato: </label>
                        <select id="GradoSltE" class="form-control btn-secondary"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="EditarUsuario();" id="BtnEditarUsu"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
