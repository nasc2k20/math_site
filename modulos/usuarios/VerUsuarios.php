<?php
require_once '../../conexion/Conectar.php';
include_once '../../conexion/Paginator.php';

$BusquedaUsu = $_REQUEST['BusquedaUsu'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if ($BusquedaUsu != '') {
  $QueryPag   = "SELECT count(*) as numrows 
                    FROM usuario a 
                    INNER JOIN grado b ON b.CodGrado=a.CodGrado 
                    WHERE a.NombreUsuario LIKE '%" . $BusquedaUsu . "%' OR a.LoginUsuario = '%". $BusquedaUsu."%'
                    ORDER BY a.NombreUsuario ASC";
} else {
  $QueryPag   = "SELECT count(*) AS numrows
                    FROM usuario";
}

$eje_usua = $Cnn->prepare($QueryPag);
$eje_usua->execute();
$dat = $eje_usua->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag'])) ? $_REQUEST['NumPag'] : 1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows / $per_page);

if ($BusquedaUsu != '') {
  $sel_usu = "SELECT * FROM usuario a 
                INNER JOIN grado b ON b.CodGrado=a.CodGrado 
                WHERE a.NombreUsuario LIKE '%" . $BusquedaUsu . "%' OR a.LoginUsuario = '%". $BusquedaUsu."%' 
                ORDER BY a.NombreUsuario ASC 
                LIMIT $offset, $per_page";
} else {
  $sel_usu = "SELECT * FROM usuario a 
                INNER JOIN grado b ON b.CodGrado=a.CodGrado 
                ORDER BY a.NombreUsuario ASC  
                LIMIT $offset, $per_page";
}


$eje_usu = $Cnn->prepare($sel_usu);
$eje_usu->execute();
?>
<table class="table table-bordered table-striped table-hover table-sm">
    <thead class="thead-dark">
        <tr>
            <th class="center">No.</th>
            <th class="center">Nombre del Usuario</th>
            <th class="center">usuario</th>
            <th class="center">Correo</th>
            <th class="center" colspan="3">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
    $finales = 0;
    while ($ver_usu = $eje_usu->fetch(PDO::FETCH_ASSOC)) {
      $UsuarioLogin = $ver_usu['LoginUsuario'];
      $UsuarioContr = $ver_usu['ContrasenaUsuario'];
      $UsuarioNombr = $ver_usu['NombreUsuario'];
      $UsuarioCorre = $ver_usu['CorreoUsuario'];
      $UsuarioEstad = $ver_usu['EstadoUsuario'];
      $NombreGrado = $ver_usu['NombreGrado'];
      $CodGrado = $ver_usu['CodGrado'];
      ?>
        <tr>
            <td width='6%' class='center'><?php echo $ContarNum; ?></td>
            <td width='50%'><?php echo $UsuarioNombr; ?></td>
            <td width='15%'><?php echo $UsuarioLogin; ?></td>
            <td width='20%'><?php echo $UsuarioCorre; ?></td>
            <td class='center' width='3%'>
                <i class="fas fa-check-square fa-2x" style="color:green;" aria-hidden="true"></i>
            </td>
            <td class='center' width='3%' data-toggle='tooltip' data-placement='top' title='Modificar'>
                <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#EditarUsuModal" data-loginusuario="<?php echo $UsuarioLogin; ?>" data-contrasusuario="<?php echo base64_decode($UsuarioContr); ?>" data-nombreusuario="<?php echo $UsuarioNombr; ?>" data-correousuario="<?php echo $UsuarioCorre; ?>" data-estadousuario="<?php echo $UsuarioEstad; ?>" data-nombregrado="<?php echo $NombreGrado; ?>" data-codgrado="<?php echo $CodGrado; ?>"><i class="fas fa-pen-square" aria-hidden="true"></i> </button>
            </td>
            <td class='center' width='3%' data-toggle="tooltip" data-placement="top" title="Eliminar">
                <button class="btn btn-outline-danger btn-sm" onclick="EliminarUsuario('<?php echo $UsuarioLogin; ?>','<?php echo $UsuarioNombr; ?>')"><i class="fas fa-trash" aria-hidden="true" title="Eliminar"></i></button>
            </td>
        </tr>
        <?php
      $finales++;
      $ContarNum++;
    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">
                <?php
        $inicios = $offset + 1;
        $finales += $inicios - 1;
        echo "Mostrando $inicios al $finales de $numrows registros";
        echo paginate($page, $total_pages, $adjacents);
        ?>
            </td>
        </tr>
    </tfoot>
</table>
