<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

if ($_POST['SqlQuery'] == 'insertar') {
	try {
		$CorreoUsuTxt		= ($_POST["CorreoUsuTxt"] == '' ? '@' : $_POST["CorreoUsuTxt"]);
		$NombreUsuTxt		= ($_POST["NombreUsuTxt"] == '' ? ' ' : strtoupper($_POST["NombreUsuTxt"]));
		$ContrasenaUsuTxt	= base64_encode($_POST["ContrasenaUsuTxt"]);
		$UsuarioTxt		    =  $_POST["UsuarioTxt"];
		$GradoSlt		    =  $_POST["GradoSlt"];
		$EstadoUsuario      = 'activo';
		$NivelUsuario = 3;

		$InserUsua = "INSERT INTO usuario (LoginUsuario, ContrasenaUsuario, NombreUsuario, CorreoUsuario, EstadoUsuario, UsuarioNivel, CodGrado)
		              VALUES(:LoginUsuario, :ContrasenaUsuario, :NombreUsuario, :CorreoUsuario, :EstadoUsuario, :UsuarioNivel, :CodGrado)";
		$InserUsua = $Cnn->prepare($InserUsua);
		$InserUsua->bindparam(":LoginUsuario", $UsuarioTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":ContrasenaUsuario", $ContrasenaUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":NombreUsuario", $NombreUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":CorreoUsuario", $CorreoUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":EstadoUsuario", $EstadoUsuario, PDO::PARAM_STR);
		$InserUsua->bindparam(":UsuarioNivel", $NivelUsuario, PDO::PARAM_STR);
		$InserUsua->bindparam(":CodGrado", $GradoSlt, PDO::PARAM_STR);

		if ($InserUsua->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Agregar los Datos:\n";
			print_r($InserUsua->errorInfo());
			echo "<br>El Codigo de Error es: " . $InserUsua->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL AGREGAR DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL AGREGAR DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL AGREGAR DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'actualizar') {
	try {
		$CorreoUsuTxt		= ($_POST["CorreoUsuTxt"] == '' ? '@' : $_POST["CorreoUsuTxt"]);
		$NombreUsuTxt		= ($_POST["NombreUsuTxt"] == '' ? ' ' : strtoupper($_POST["NombreUsuTxt"]));
		$ContrasenaUsuTxt	= base64_encode($_POST["ContrasenaUsuTxt"]);
		$UsuarioTxt		    =  $_POST["UsuarioTxt"];
		$GradoSltE		    =  $_POST["GradoSltE"];

		$UpdaUsua = "UPDATE usuario 
					SET 
					ContrasenaUsuario=:ContrasenaUsuario, 
					NombreUsuario=:NombreUsuario, 
					CorreoUsuario=:CorreoUsuario, 
					CodGrado=:CodGrado 
					WHERE LoginUsuario=:LoginUsuario";
		$UpdaUsua = $Cnn->prepare($UpdaUsua);
		$UpdaUsua->bindparam(":ContrasenaUsuario", $ContrasenaUsuTxt, PDO::PARAM_STR);
		$UpdaUsua->bindparam(":NombreUsuario", $NombreUsuTxt, PDO::PARAM_STR);
		$UpdaUsua->bindparam(":CorreoUsuario", $CorreoUsuTxt, PDO::PARAM_STR);
		$UpdaUsua->bindparam(":LoginUsuario", $UsuarioTxt, PDO::PARAM_STR);
		$UpdaUsua->bindparam(":CodGrado", $GradoSltE, PDO::PARAM_STR);

		if ($UpdaUsua->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Actualizar los Datos:\n";
			print_r($UpdaUsua->errorInfo());
			echo "<br>El Codigo de Error es: " . $UpdaUsua->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ACTUALIZAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'eliminar') {

	try {
		$UsuarioTxt		    =  $_POST["UsuarioTxt"];

		$DeleUsua = "DELETE FROM usuario WHERE LoginUsuario=:LoginUsuario";
		$DeleUsua = $Cnn->prepare($DeleUsua);
		$DeleUsua->bindParam(":LoginUsuario", $UsuarioTxt, PDO::PARAM_STR);


		if ($DeleUsua->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Eliminar los Datos:\n";
			print_r($DeleUsua->errorInfo());
			echo "<br>El Codigo de Error es: " . $DeleUsua->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ELIMINAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ELIMINAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ELIMINAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} else { }
