<?php
include 'AgregarUsuModal.php';
include 'EditarUsuModal.php';
?>

<section>
    <div class="row">
        <div class="col">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Busqueda por Usuario o Nombre de Alumno" id="BusquedaTxt" style="text-transform: uppercase;">
                <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                </span>
            </div>
        </div>
        <div class="col text-right">
            <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarUsuModal"><i class="fas fa-plus-square" aria-hidden="true"></i> </button>
        </div>
    </div>

</section>

<div style="height:10px;"></div>

<section>
    <div class="box box-primary">
        <div class="box-body">
            <div id="MostrarDatosUsuario"></div>
        </div>
    </div>
</section>

<script src="usuarios/UsuariosQuerys.js"></script>