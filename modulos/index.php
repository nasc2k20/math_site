<?php
include '../login/ValidarSesion.php';
include_once '../conexion/Conectar.php';
$CodGrado = $_SESSION['CodGrado'];

$SelGrado = "SELECT * FROM grado WHERE CodGrado='" . $CodGrado . "'";
$EjeGrado = $Cnn->prepare($SelGrado);
$EjeGrado->execute();
$VerGrado = $EjeGrado->fetch(PDO::FETCH_ASSOC);
$NombreGrado = $VerGrado['NombreGrado'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="../styles/css/all.min.css">
    <link rel="stylesheet" href="../styles/css/jquery-confirm.min.css">
    <link rel="stylesheet" href="../styles/css/smoothbox.css">
    <title>MathInfo</title>
    <link rel="icon" type="image/png" href="../login/images/icons/favicon.ico" />
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">MathInfo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <?php if ($_SESSION["UsuarioNivel"] == 0) { ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Matenimientos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item VerAreasItem" href="#">Areas</a>
                            <a class="dropdown-item VerEjerciciosItem" href="#">Ejercicios</a>
                            <a class="dropdown-item VerUsuariosItem" href="#">Usuarios</a>
                            <a class="dropdown-item VerGradosItem" href="#">Grados</a>
                        </div>
                    </li>
                <?php } ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Opciones
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item VerMiPerfilItem" href="#">Mi Perfil</a>
                        <a class="dropdown-item" href="../login/CerrarSesion.php">Cerrar Sesi&oacute;n</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <section>
        <div class="container-fluid">
            <div class="alert alert-info" role="alert">
                <?php
                echo "Bienvenid@: " . $_SESSION['NombreUsuario'] . "<br>";
                echo $NombreGrado;
                ?>
            </div>
            <div style="height: 2rem;"></div>
            <?php if ($_SESSION["UsuarioNivel"] == 0) { ?>
                <div id="CargadorDatos"></div>
            <?php } else { ?>
                <div id="CargadorDatosAlumno"></div>
            <?php } ?>
            <div id="CargarMiPerfilDv"></div>
    </section>

</body>
<script src="../styles/js/jquery-3.4.1.js" charset="utf-8"></script>
<script src="../styles/js/bootstrap.bundle.min.js" charset="utf-8"></script>
<script src="../styles/js/all.min.js" charset="utf-8"></script>
<script src="../styles/js/jquery-confirm.min.js" charset="utf-8"></script>
<script src="../styles/js/smoothbox.min.js" charset="utf-8"></script>
<script src="../styles/js/CargarDivs.js" charset="utf-8"></script>
<script src="CargarRespuestas.js" charset="utf-8"></script>

</html>