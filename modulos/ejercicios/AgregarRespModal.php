
   <div class="modal fade" id="AsignarRespuestaModal" tabindex="-1" role="dialog" aria-labelledby="AsignarRespuestaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AsignarRespuestaModalLabel">Asignar Respuesta al Ejercicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="ejercicios/InsertarImg.php" id="RespuestaForm" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="SqlQuery" value="insertarResp">
                        <input type="hidden" id="CodEjercicioTxt" name="CodEjercicioTxt">
                        <label for="RespuestaRapTxt">Respuesta R&aacute;pida: </label>
                        <input type="text" class="form-control" id="RespuestaRapTxt" name="RespuestaRapTxt" placeholder="Respuesta Rapida" style="text-transform:uppercase;">
                    </div>
                    <div class="form-group">
                        <label for="ProcedimientoRespTxt">Procedimiento: </label>
                        <input type="file" id="ProcedimientoRespTxt" name="ProcedimientoRespTxt" class="form-control-file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                    <button type="submit" class="btn btn-success" id="BtnAgregarResp" name="BtnAgregarResp"><i class="fas fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--
<div class="modal fade" id="AsignarRespuestaModal" tabindex="-1" role="dialog" aria-labelledby="AsignarRespuestaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AsignarRespuestaModalLabel">Asignar Respuesta al Ejercicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" enctype="multipart/form-data" name="FormArchivo" id="FormArchivo">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" id="SqlQuery" value="insertarResp">
                        <input type="text" id="CodEjercicioTxt" name="CodEjercicioTxt">
                        <label for="RespuestaRapTxt">Respuesta R&aacute;pida: </label>
                        <input type="text" class="form-control" id="RespuestaRapTxt" name="RespuestaRapTxt" placeholder="Respuesta Rapida" style="text-transform:uppercase;">
                    </div>
                    <div class="form-group">
                        <label for="ProcedimientoRespTxt">Procedimiento: </label>
                        <input type="file" id="ProcedimientoRespTxt" name="ProcedimientoRespTxt" multiple class="form-control-file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                    <button type="button" class="btn btn-success" id="BtnAgregarResp" name="BtnAgregarResp"><i class="fas fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
-->