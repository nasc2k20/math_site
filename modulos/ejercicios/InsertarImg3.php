<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

//print_r($_FILES);


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    echo "REQUEST_METHOD";
    //if(isset($_POST['BtnAgregarResp']))
    if(isset($_FILES['ProcedimientoRespTxt']))    
    {
        echo "Aqui";
        $CodEjercicioTxt     = $_POST['CodEjercicioTxt'];
        $RespuestaRapTxt     = $_POST['RespuestaRapTxt'];
        $RespEjercicio = 1;
        
        $Errores = [];
        $DirectorioImg = '../../imgresp/';
        $ExtensionesArc = ['jpg', 'jpeg', 'png'];
        
        $TodosArchivos = count($_FILES['ProcedimientoRespTxt']['tmp_name']);
        
        for($i=0; $i<$TodosArchivos; $i++)
        {
            $ArchivoNombre = $_FILES['ProcedimientoRespTxt']['name'][$i];
            $ArchivoTempor = $_FILES['ProcedimientoRespTxt']['tmp_name'][$i];
            $ArchivoTipo = $_FILES['ProcedimientoRespTxt']['type'][$i];
            $ArchivoTam = $_FILES['ProcedimientoRespTxt']['size'][$i];
            
            $ArchivoExten = strtolower(end(explode('.',$_FILES['ProcedimientoRespTxt']['name'][$i])));
            
            $ArchivoSubir = $DirectorioImg.$ArchivoNombre;
            
            if(!in_array($ArchivoExten, $ExtensionesArc)){
                $Errores[] = "Extension No Permitida: ". $ArchivoNombre.' '. $ArchivoTipo;
            }
            
            if($ArchivoTam>50000)
            {
                $Errores[] = "Archivo Excede el Tamaño Permitido: ". $ArchivoNombre.' '.$ArchivoTipo;
            }
            
            if(empty($Errores))
            {
                
                move_uploaded_file($ArchivoTempor, $ArchivoSubir);
                
                try {
                $InserResp = "INSERT INTO respuesta (CodEjercicio, DesarrolloRespuesta, RespuestaRapida) 
                                VALUES (:CodEjercicio, :DesarrolloRespuesta, :RespuestaRapida)";
                $InserResp = $Cnn->prepare($InserResp);
                $InserResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);
                $InserResp->bindParam(":DesarrolloRespuesta", $ArchivoImagen, PDO::PARAM_STR);
                $InserResp->bindParam(":RespuestaRapida", $RespuestaRapTxt, PDO::PARAM_STR);


                $UpdaResp = "UPDATE ejercicio SET RespEjercicio=:RespEjercicio WHERE CodEjercicio=:CodEjercicio";
                $UpdaResp = $Cnn->prepare($UpdaResp);
                $UpdaResp->bindParam(":RespEjercicio", $RespEjercicio, PDO::PARAM_STR);
                $UpdaResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);

                if ($InserResp->execute()) {
                        $UpdaResp->execute();
                        //echo "<script>location.href='../../index.php#CargadorDatos'</script>";
                    } else {
                        echo "\nError al Insertar los Datos:\n";
                        print_r($InserResp->errorInfo());
                        echo "<br>El Codigo de Error es: " . $InserResp->errorCode();
                    }
                } catch (PDOException $e) {
                    echo "ERROR AL INSERTAR RESPUESTA LOS DATOS " . $e->getMessage();
                    exit;
                } catch (Throwable $t) {
                    echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 2 " . $t->getMessage();
                    exit;
                } catch (Exception $s) {
                    echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 3 " . $s->getMessage();
                    exit;
                }
            }
        }
        if($Errores) print_r($Errores);
    }
}
/**/
/*
if (isset($_POST['BtnAgregarResp'])) {
    $CodEjercicioTxt     = $_POST['CodEjercicioTxt'];
    $RespuestaRapTxt     = $_POST['RespuestaRapTxt'];    

    $ArchivoImagen = $_FILES['ProcedimientoRespTxt']['name'];
    $ArchivoTempor = $_FILES['ProcedimientoRespTxt']['tmp_name'];
    //$DirectorioImg = $_SERVER['DOCUMENT_ROOT'] . '/matematicas/imgResp/';
    $DirectorioImg = '../../imgresp/';

    $ArchivoImagen = rand(1000,1000000).$ArchivoImagen;

    $DirectorioCompleto = $DirectorioImg.$ArchivoImagen;

    move_uploaded_file($_FILES['ProcedimientoRespTxt']['tmp_name'],$DirectorioImg.$ArchivoImagen);
    
    try {
        
        $RespEjercicio = 1;
        $InserResp = "INSERT INTO respuesta (CodEjercicio, DesarrolloRespuesta, RespuestaRapida) 
                        VALUES (:CodEjercicio, :DesarrolloRespuesta, :RespuestaRapida)";
        $InserResp = $Cnn->prepare($InserResp);
        $InserResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);
        $InserResp->bindParam(":DesarrolloRespuesta", $ArchivoImagen, PDO::PARAM_STR);
        $InserResp->bindParam(":RespuestaRapida", $RespuestaRapTxt, PDO::PARAM_STR);
        
        
        $UpdaResp = "UPDATE ejercicio SET RespEjercicio=:RespEjercicio WHERE CodEjercicio=:CodEjercicio";
        $UpdaResp = $Cnn->prepare($UpdaResp);
        $UpdaResp->bindParam(":RespEjercicio", $RespEjercicio, PDO::PARAM_STR);
        $UpdaResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);

        if ($InserResp->execute()) {
                $UpdaResp->execute();
                echo "<script>location.href='../../index.php#CargadorDatos'</script>";
            } else {
                echo "\nError al Insertar los Datos:\n";
                print_r($InserResp->errorInfo());
                echo "<br>El Codigo de Error es: " . $InserResp->errorCode();
            }
    } catch (PDOException $e) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS " . $e->getMessage();
        exit;
    } catch (Throwable $t) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 2 " . $t->getMessage();
        exit;
    } catch (Exception $s) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 3 " . $s->getMessage();
        exit;
    }
}
*/