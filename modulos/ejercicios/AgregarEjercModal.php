<div class="modal fade" id="AgregarEjercicioModal" tabindex="-1" role="dialog" aria-labelledby="AgregarEjercicioModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AgregarEjercicioModalLabel">Nueva Ejercicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="GradoEjercSlt">Seleccione Bachillerato: </label>
                    <select id="GradoEjercSlt" class="form-control btn-secondary"></select>
                </div>
                <div class="form-group">
                    <label for="AreasEjercSlt">Area: </label>
                    <select id="AreasEjercSlt" class="form-control btn-secondary"></select>
                </div>
                <hr>
                <div class="form-group">
                    <label for="DescripcionEjercTxt">Descripci&oacute;n: </label>
                    <input type="text" class="form-control" id="DescripcionEjercTxt" placeholder="Tema del Ejercicio" style="text-transform:uppercase;">
                </div>
                <div class="form-group">
                    <label for="EnunciadoEjercTxt">Enunciado: </label>
                    <textarea id="EnunciadoEjercTxt" class="form-control" cols="30" rows="10" style="resize:none;" placeholder="Enunciado"></textarea>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="AgregarEjercicio();" id="BtnAgregarEjerc"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>