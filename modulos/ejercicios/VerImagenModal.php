<div class="modal fade" id="MostrarImagenModal" tabindex="-1" role="dialog" aria-labelledby="MostrarImagenModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="MostrarImagenModalTitle">Visualizar Imagen</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"><img class="img-fluid rounded mx-auto d-block" id="MostrarImagenDiv" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
      </div>
    </div>
  </div>
</div>