<?php
include 'AgregarEjercModal.php';
include 'EditarEjercModal.php';
include 'AgregarRespModal.php';
include 'VerImagenModal.php';
?>

<section>
    <div class="row">
        <div class="col">
            <label for="">Seleccione Area</label>
            <select id="AreaSlt" class="form-control btn-secondary"></select>
        </div>
        <div class="col text-right">
            <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarEjercicioModal"><i class="fas fa-plus-square" aria-hidden="true"></i> </button>
        </div>
    </div>

</section>

<div style="height:10px;"></div>

<section>
    <div class="box box-primary">
        <div class="box-body">
            <div id="MostrarDatosEjercicios"></div>
        </div>
    </div>
</section>

<script src="ejercicios/EjercicioQuerys.js"></script>