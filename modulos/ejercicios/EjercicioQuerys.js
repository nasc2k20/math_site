var DirectorioEjerc = 'ejercicios/';

$(function () {

    $('#AgregarEjercicioModal').on('shown.bs.modal', function () {
        $('#DescripcionEjercTxt').focus();
    });

    $.post(DirectorioEjerc + "CargarAreaComboBuscar.php", function (data) {
        $("#AreaSlt").html(data);
    });

    $.post(DirectorioEjerc + "CargarAreaCombo.php", function (data) {
        $("#AreasEjercSlt").html(data);
    });

    $.post(DirectorioEjerc + "CargarAreaCombo.php", function (data) {
        $("#AreasEjercSltE").html(data);
    });

    $.post("usuarios/LlenarComboGrado.php", function(datos){
        $('#GradoEjercSlt').html(datos).fadeIn('slow');
    });

    $.post("usuarios/LlenarComboGrado.php", function(datos){
        $('#GradoEjercSltE').html(datos).fadeIn('slow');
    });

    CargarProcesos(1);

    $('#AreaSlt').change(function () {
        var id = $(this).find(":selected").val();
        CargarProcesos(1);
    });

    $('#EditarEjercModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var DespcricionEjercTxt = button.data('descripcionejercicio');
        $('#DescripcionEjercTxtE').val(DespcricionEjercTxt);
        var CodArea = button.data('codearea');
        $('#AreasEjercSltE').val(CodArea);
        var CodGrado = button.data('codgrado');
        $('#GradoEjercSltE').val(CodGrado);
        var EnunciadoEjerc = button.data('enunciadoejercicio');
        $('#EnunciadoEjercTxtE').val(EnunciadoEjerc);
        var CodEjercicio = button.data('codeejercicio');
        $('#CodEjercicioTxtE').val(CodEjercicio);
    });

    $('#AsignarRespuestaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var CodEjercicio = button.data('codeejercicio');
        $('#CodEjercicioTxt').val(CodEjercicio);
    });
    
    $('#MostrarImagenModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var NombreImg = button.data('nombreimagen');
        var Ruta = '../../imgresp/';
        $('#MostrarImagenDiv').attr('src', Ruta + NombreImg);
    });
    
});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var CodeArea = ($('#AreaSlt option:selected').val() > 0 ? $('#AreaSlt option:selected').val() : 'MostrarTodos');

    var Datos = {
        "CodeArea": CodeArea,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioEjerc + "VerEjercicios.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosEjercicios").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosEjercicios').html(data).fadeIn('slow');
        }
    });
}

function AgregarEjercicio() {
    var DescripcionEjercTxt = $('#DescripcionEjercTxt').val();
    var AreasEjercSlt = $('#AreasEjercSlt option:selected').val();
    var GradoEjercSlt = $('#GradoEjercSlt option:selected').val();
    var EnunciadoEjercTxt = $('#EnunciadoEjercTxt').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "DescripcionEjercTxt": DescripcionEjercTxt,
        "EnunciadoEjercTxt": EnunciadoEjercTxt,
        "GradoEjercSlt": GradoEjercSlt,
        "AreasEjercSlt": AreasEjercSlt
    };

    if (DescripcionEjercTxt == '') {
        alert('Debe de Digitar la Descripcion del Ejercicio');
        $('#DescripcionEjercTxt').focus();
    } else if (EnunciadoEjercTxt == '') {
        alert('Debe de Digitar el Enunciado del Ejercicio');
        $('#EnunciadoEjercTxt').focus();
    } else if (AreasEjercSlt == '') {
        alert('Debe de Seleccionar el Area');
        $('#AreasEjercSlt').focus();
    } else if (GradoEjercSlt == '') {
        alert('Debe de Seleccionar el Grado');
        $('#GradoEjercSlt').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioEjerc + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosEjercicios").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosEjercicios").html(datos);
                CargarProcesos(1);
                $('#AgregarEjercicioModal').modal('hide');

            }
        });
    }

    event.preventDefault();
}


function EditarEjercicio() {
    var DescripcionEjercTxtE = $('#DescripcionEjercTxtE').val();
    var AreasEjercSltE = $('#AreasEjercSltE option:selected').val();
    var GradoEjercSltE = $('#GradoEjercSltE option:selected').val();
    var EnunciadoEjercTxtE = $('#EnunciadoEjercTxtE').val();
    var CodEjercicioTxtE = $('#CodEjercicioTxtE').val();
    var SqlQuery = 'actualizar';

    var Datos = {
        "DescripcionEjercTxtE": DescripcionEjercTxtE,
        "AreasEjercSltE": AreasEjercSltE,
        "EnunciadoEjercTxtE": EnunciadoEjercTxtE,
        "CodEjercicioTxtE": CodEjercicioTxtE,
        "GradoEjercSltE": GradoEjercSltE,
        "SqlQuery": SqlQuery
    };

    if (DescripcionEjercTxtE == '') {
        alert('Debe de Digitar la Descripcion del Ejercicio');
        $('#DescripcionEjercTxtE').focus();
    } else if (EnunciadoEjercTxtE == '') {
        alert('Debe de Digitar el Enunciado del Ejercicio');
        $('#EnunciadoEjercTxtE').focus();
    } else if (AreasEjercSltE == '') {
        alert('Debe de Seleccionar el Area');
        $('#AreasEjercSltE').focus();
    } else if (GradoEjercSltE == '') {
        alert('Debe de Seleccionar el Grado');
        $('#GradoEjercSltE').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioEjerc + "ProcesosSQL.php",
            data: Datos,
            beforeSend: function (objeto) {
                $("#MostrarDatosEjercicios").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosEjercicios").html(datos);
                CargarProcesos(1);
                $('#EditarEjercModal').modal('hide');
            }
        });
    }


    event.preventDefault();
}

function EliminarEjercicio(CodeEjercicio, DescripcionEjerc) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "CodeEjercicio": CodeEjercicio
    }

    $.confirm({
        icon: 'fas fa-smile',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Ejercicio: <br> <strong><h3>' + DescripcionEjerc + '</h3></strong>',
        animation: 'scale',
        icon: 'fas fa-exclamation-triangle',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioEjerc + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosEjercicios").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosEjercicios").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}

function AgregarRespuesta() {
    //var CodEjercicioTxt = document.getElementById('CodEjercicioTxt').value;
    //var RespuestaRapTxt = document.getElementById('RespuestaRapTxt').value;
    var CodEjercicioTxt = $('#CodEjercicioTxt').val();
    var RespuestaRapTxt = $('#RespuestaRapTxt').val();
    //var ProcedimientoRespTxt = $('#ProcedimientoRespTxt')[0].files[0]['name'];
    var ProcedimientoRespTxt = $('#ProcedimientoRespTxt').val();

    alert(ProcedimientoRespTxt);
    /*const Parametros = new FormData();
    Parametros.append('CodEjercicioTxt', CodEjercicioTxt);
    Parametros.append('RespuestaRapTxt', RespuestaRapTxt);
    Parametros.append('ProcedimientoRespTxt', ProcedimientoRespTxt);*/

    var Datos = {
        "CodEjercicioTxt": CodEjercicioTxt,
        "RespuestaRapTxt": RespuestaRapTxt,
        "ProcedimientoRespTxt": ProcedimientoRespTxt
    };

    $.ajax({
        type: 'POST',
        url: DirectorioEjerc + 'InsertarImg2.php',
        data: Datos,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
        }
    });
}

function AgregarRespuesta2() {
    const Direccion = DirectorioEjerc + 'InsertarImg.php';
    const Formulario = document.querySelector('form');
    //var CodEjercicioTxt = $('#CodEjercicioTxt').val();
    //var RespuestaRapTxt = $('#RespuestaRapTxt').val();
    var CodEjercicioTxt = document.getElementById('CodEjercicioTxt').value;
    var RespuestaRapTxt = document.getElementById('RespuestaRapTxt').value;

    Formulario.addEventListener('submit', e => {
        e.preventDefault();

        const Archivo = document.querySelector('[type=file]').files;
        const Parametros = new FormData();

        for (let i = 0; i < Archivo.length; i++) {
            let File = Archivo[i];

            Parametros.append('Archivo[]', File);
        }
        Parametros.append('CodEjercicioTxt', CodEjercicioTxt);
        Parametros.append('RespuestaRapTxt', RespuestaRapTxt);

        fetch(Direccion, {
                method: 'POST',
                body: Parametros
            })
            .then(function (Respuesta) {
                console.log(Respuesta);
            })
            .catch(function (Error) {
                consol.log(Error);
            })
    });
}
