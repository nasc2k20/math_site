<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

if(!empty($_FILES["ProcedimientoRespTxt"]["name"]))
{
    $NombreArchivo = time().'_'.$_FILES['ProcedimientoRespTxt']['name'];
    $ExtensionesVal = array("jpeg", "jpg", "png");
    $ArchivoTemp = explode(".", $_FILES["ProcedimientoRespTxt"]["name"]);
    $ExtensionArchivo = end($ArchivoTemp);
    $DirectorioImg = '../../imgresp/';
    if((($_FILES["ProcedimientoRespTxt"]["type"] == "image/png") || ($_FILES["ProcedimientoRespTxt"]["type"] == "image/jpg") || ($_FILES["ProcedimientoRespTxt"]["type"] == "image/jpeg")) && in_array($ExtensionArchivo, $ExtensionesVal)) {
        $ArchivoDir = $_FILES['ProcedimientoRespTxt']['tmp_name'];
        $ArchivoSubir = $DirectorioImg.$NombreArchivo;
        if(move_uploaded_file($ArchivoDir,$ArchivoSubir)) {
            $ArchivoFinalNombre = $NombreArchivo;
        } else {
            $ArchivoFinalNombre = '';
        }
    } else {
        $json['error']['file'] = 'Please choose valid file';
    }
} else {
    $json['error']['file'] = 'Please choose image';
}

if(empty($json['error'])) {
    $DirectorioImg = '../../imgresp/';
    $CodEjercicioTxt     = $_POST['CodEjercicioTxt'];
    $RespuestaRapTxt     = $_POST['RespuestaRapTxt'];
    $RespEjercicio = 1;
    
    try {        
        $InserResp = "INSERT INTO respuesta (CodEjercicio, DesarrolloRespuesta, RespuestaRapida) 
                        VALUES (:CodEjercicio, :DesarrolloRespuesta, :RespuestaRapida)";
        $InserResp = $Cnn->prepare($InserResp);
        $InserResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);
        $InserResp->bindParam(":DesarrolloRespuesta", $ArchivoFinalNombre, PDO::PARAM_STR);
        $InserResp->bindParam(":RespuestaRapida", $RespuestaRapTxt, PDO::PARAM_STR);
        
        
        $UpdaResp = "UPDATE ejercicio SET RespEjercicio=:RespEjercicio WHERE CodEjercicio=:CodEjercicio";
        $UpdaResp = $Cnn->prepare($UpdaResp);
        $UpdaResp->bindParam(":RespEjercicio", $RespEjercicio, PDO::PARAM_STR);
        $UpdaResp->bindParam(":CodEjercicio", $CodEjercicioTxt, PDO::PARAM_STR);

        if ($InserResp->execute()) {
                $UpdaResp->execute();
                //echo "<script>location.href='../../index.php#CargadorDatos'</script>";
            } else {
                echo "\nError al Insertar los Datos:\n";
                print_r($InserResp->errorInfo());
                echo "<br>El Codigo de Error es: " . $InserResp->errorCode();
            }
    } catch (PDOException $e) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS " . $e->getMessage();
        exit;
    } catch (Throwable $t) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 2 " . $t->getMessage();
        exit;
    } catch (Exception $s) {
        echo "ERROR AL INSERTAR RESPUESTA LOS DATOS 3 " . $s->getMessage();
        exit;
    }
    
    $json['ProcedimientoRespTxt'] = $DirectorioImg.$ArchivoFinalNombre;
    $json['msg'] = 'success';
} 
echo json_encode($json);
?>
