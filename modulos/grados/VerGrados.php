<?php
require_once '../../conexion/Conectar.php';
include_once '../../conexion/Paginator.php';

$BusquedaGrado = $_REQUEST['BusquedaGrado'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if ($BusquedaGrado != '') {
  $QueryPag   = "SELECT count(*) as numrows 
                    FROM grado 
                    WHERE NombreGrado LIKE '%" . $BusquedaGrado . "%' 
                    ORDER BY NombreGrado ASC";
} else {
  $QueryPag   = "SELECT count(*) AS numrows
                    FROM grado";
}

$eje_grado = $Cnn->prepare($QueryPag);
$eje_grado->execute();
$dat = $eje_grado->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag'])) ? $_REQUEST['NumPag'] : 1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows / $per_page);

if ($BusquedaGrado != '') {
  $sel_grado = "SELECT * FROM grado
                WHERE NombreGrado LIKE '%" . $BusquedaGrado . "%' 
                ORDER BY NombreGrado ASC 
                LIMIT $offset, $per_page";
} else {
  $sel_grado = "SELECT * FROM grado ORDER BY NombreGrado ASC  
                LIMIT $offset, $per_page";
}


$eje_grado = $Cnn->prepare($sel_grado);
$eje_grado->execute();
?>
<table class="table table-bordered table-striped table-hover table-sm">
  <thead class="thead-dark">
    <tr>
      <th class="center">No.</th>
      <th class="center">Nombre del Grado</th>
      <th class="center" colspan="2">Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $finales = 0;
    while ($ver_grado = $eje_grado->fetch(PDO::FETCH_ASSOC)) {
      $NombreGrado = $ver_grado['NombreGrado'];
      $CodGrado = $ver_grado['CodGrado'];
      ?>
      <tr>
        <td width='4%' class='center'><?php echo $ContarNum; ?></td>
        <td width='90%'><?php echo $NombreGrado; ?></td>
        <td class='center' width='3%' data-toggle='tooltip' data-placement='top' title='Modificar'>
        <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#EditarGradoModal" data-nombregrado="<?php echo $NombreGrado; ?>" data-codgrado="<?php echo $CodGrado; ?>"><i class="fas fa-pen-square" aria-hidden="true"></i> </button>
        </td>
        <td class='center' width='3%' data-toggle="tooltip" data-placement="top" title="Eliminar">
          <button class="btn btn-outline-danger btn-sm" onclick="EliminarGrado('<?php echo $CodGrado; ?>','<?php echo $NombreGrado; ?>')"><i class="fas fa-trash" aria-hidden="true" title="Eliminar"></i></button>
        </td>
      </tr>
      <?php
      $finales++;
      $ContarNum++;
    }
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">
        <?php
        $inicios = $offset + 1;
        $finales += $inicios - 1;
        echo "Mostrando $inicios al $finales de $numrows registros";
        echo paginate($page, $total_pages, $adjacents);
        ?>
      </td>
    </tr>
  </tfoot>
</table>