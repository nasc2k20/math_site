var DirectorioGrado = 'grados/';

$(function () {
    $('#AgregarGradoModal').on('shown.bs.modal', function () {
        $('#NombreGradoTxt').focus();
        document.getElementById('NombreGradoTxt').select();
    });

    CargarProcesos(1);

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

    $('#EditarGradoModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var NombreGrado = button.data('nombregrado');
        $('#NombreGradoTxtE').val(NombreGrado);
        var CodGrado = button.data('codgrado');
        $('#CodGradoTxtE').val(CodGrado);
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 15;
    var BusquedaGrado = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaGrado": BusquedaGrado,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioGrado + "VerGrados.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosGrado").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosGrado').html(data).fadeIn('slow');
        }
    });
}

function AgregarGrado() {
    var NombreGrado = $('#NombreGradoTxt').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "NombreGrado": NombreGrado
    };

    if (NombreGrado == '') {
        alert('Debe de Digitar el Nombre del Grado');
        $('#NombreGradoTxt').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioGrado + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosGrado").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosGrado").html(datos);
                CargarProcesos(1);
                $('#AgregarGradoModal').modal('hide');
                /*
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                */

            }
        });
    }

    event.preventDefault();
}


function EditarGrado() {
    var NombreGradoTxtE = $('#NombreGradoTxtE').val();
    var CodGradoTxtE = $('#CodGradoTxtE').val();
    var SqlQuery = 'actualizar';

    var Datos = {
        "NombreGradoTxtE": NombreGradoTxtE,
        "CodGradoTxtE": CodGradoTxtE,
        "SqlQuery": SqlQuery
    };

    if (NombreGradoTxtE == '') {
        alert('Debe de Digitar el Nombre del Grado');
        $('#NombreGradoTxtE').focus();
    } else if (CodGradoTxtE <=0) {
        alert('El codigo del Grado es Invalido');
        $('#NombreGradoTxtE').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioGrado + "ProcesosSQL.php",
            data: Datos,
            beforeSend: function (objeto) {
                $("#MostrarDatosGrado").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $("#MostrarDatosGrado").html(datos);
                CargarProcesos(1);
                $('#EditarGradoModal').modal('hide');
            }
        });
    }


    event.preventDefault();
}

function EliminarGrado(CodGrado, NombreGrado) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "CodGrado": CodGrado
    }

    $.confirm({
        icon: 'fas fa-smile',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Grado: <br> <strong><h3>' + NombreGrado + '</h3></strong>',
        animation: 'scale',
        icon: 'fas fa-exclamation-triangle',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioGrado + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosGrado").html("<center><img src='../images/30.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosGrado").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}