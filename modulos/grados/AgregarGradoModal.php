<div class="modal fade" id="AgregarGradoModal" tabindex="-1" role="dialog" aria-labelledby="AgregarGradoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AgregarGradoModalLabel">Nuevo Grado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="NombreGradoTxt">Nombre del Grado: </label>
                    <input type="text" class="form-control" id="NombreGradoTxt" placeholder="Nombre del Grado" style="text-transform:uppercase;">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="AgregarGrado();" id="BtnAgregarGrado"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>