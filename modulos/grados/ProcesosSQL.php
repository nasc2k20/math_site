<?php
require_once '../../conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");

if ($_POST['SqlQuery'] == 'insertar') {
	try {
		$NombreGrado		= ($_POST["NombreGrado"] == '' ? ' ' : strtoupper($_POST["NombreGrado"]));


		$InserGrado = "INSERT INTO grado (NombreGrado)
					VALUES(:NombreGrado)";
		$InserGrado = $Cnn->prepare($InserGrado);
		$InserGrado->bindparam(":NombreGrado", $NombreGrado, PDO::PARAM_STR);

		if ($InserGrado->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Agregar los Datos:\n";
			print_r($InserGrado->errorInfo());
			echo "<br>El Codigo de Error es: " . $InserGrado->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL AGREGAR DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL AGREGAR DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL AGREGAR DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'actualizar') {
	try {
		$CodGradoE		= $_POST["CodGradoTxtE"];
		$NombreGrado		= ($_POST["NombreGradoTxtE"] == '' ? ' ' : strtoupper($_POST["NombreGradoTxtE"]));


		$UpdaGrado = "UPDATE grado 
						SET 
						NombreGrado=:NombreGrado 
						WHERE CodGrado=:CodGrado";
		$UpdaGrado = $Cnn->prepare($UpdaGrado);
		$UpdaGrado->bindparam(":CodGrado", $CodGradoE, PDO::PARAM_STR);
		$UpdaGrado->bindparam(":NombreGrado", $NombreGrado, PDO::PARAM_STR);

		if ($UpdaGrado->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Actualizar los Datos:\n";
			print_r($UpdaGrado->errorInfo());
			echo "<br>El Codigo de Error es: " . $UpdaGrado->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ACTUALIZAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ACTUALIZAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} elseif ($_POST['SqlQuery'] == 'eliminar') {

	try {
		$CodGradoE     = $_POST['CodGrado'];

		$DeleGrado = "DELETE FROM grado WHERE CodGrado=:CodGrado";
		$DeleGrado = $Cnn->prepare($DeleGrado);
		$DeleGrado->bindParam(":CodGrado", $CodGradoE, PDO::PARAM_STR);


		if ($DeleGrado->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Eliminar los Datos:\n";
			print_r($DeleGrado->errorInfo());
			echo "<br>El Codigo de Error es: " . $DeleGrado->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL ELIMINAR LOS DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL ELIMINAR LOS DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL ELIMINAR LOS DATOS 3 " . $s->getMessage();
		exit;
	}
} else { }
