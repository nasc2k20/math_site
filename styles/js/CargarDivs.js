$(function () {
    $('#CargadorDatosAlumno').load('respuestas/index.php');

    $('.VerAreasItem').click(function(){
        $('#CargadorDatos').load('areas/index.php');
    });
    
    $('.VerEjerciciosItem').click(function(){
        $('#CargadorDatos').load('ejercicios/index.php');
    });
    
    $('.VerUsuariosItem').click(function(){
        $('#CargadorDatos').load('usuarios/index.php');
    });
    
    $('.VerGradosItem').click(function(){
        $('#CargadorDatos').load('grados/index.php');
    });
    
    $('.VerMiPerfilItem').click(function(){
        $('#CargarMiPerfilDv').load('../VerMiPerfil.php', function(){
            $('#CargadorDatos').hide();
            $('#CargadorDatosAlumno').hide();
        });
    });
});