<?php
require_once 'conexion/Conectar.php';

date_default_timezone_set("America/El_Salvador");
if ($_POST['SqlQuery'] == 'insertar') {
	try {
		$CorreoUsuTxt		= ($_POST["CorreoUsuTxt"] == '' ? '@' : $_POST["CorreoUsuTxt"]);
		$NombreUsuTxt		= ($_POST["NombreUsuTxt"] == '' ? ' ' : strtoupper($_POST["NombreUsuTxt"]));
		$ContrasenaUsuTxt	= base64_encode($_POST["ContrasenaUsuTxt"]);
		$UsuarioTxt		    =  $_POST["UsuarioTxt"];
		$CodGrado		    =  $_POST["CodGrado"];
		$EstadoUsuario      = 'activo';
		$NivelUsuario = 3;

		$InserUsua = "INSERT INTO usuario (LoginUsuario, ContrasenaUsuario, NombreUsuario, CorreoUsuario, EstadoUsuario, UsuarioNivel, CodGrado)
		              VALUES(:LoginUsuario, :ContrasenaUsuario, :NombreUsuario, :CorreoUsuario, :EstadoUsuario, :UsuarioNivel, :CodGrado)";
		$InserUsua = $Cnn->prepare($InserUsua);
		$InserUsua->bindparam(":LoginUsuario", $UsuarioTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":ContrasenaUsuario", $ContrasenaUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":NombreUsuario", $NombreUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":CorreoUsuario", $CorreoUsuTxt, PDO::PARAM_STR);
		$InserUsua->bindparam(":EstadoUsuario", $EstadoUsuario, PDO::PARAM_STR);
		$InserUsua->bindparam(":UsuarioNivel", $NivelUsuario, PDO::PARAM_STR);
		$InserUsua->bindparam(":CodGrado", $CodGrado, PDO::PARAM_STR);

		if ($InserUsua->execute()) {
			header("location: index.php");
		} else {
			echo "\nError al Agregar los Datos:\n";
			print_r($InserUsua->errorInfo());
			echo "<br>El Codigo de Error es: " . $InserUsua->errorCode();
		}
	} catch (PDOException $e) {
		echo "ERROR AL AGREGAR DATOS " . $e->getMessage();
		exit;
	} catch (Throwable $t) {
		echo "ERROR AL AGREGAR DATOS 2 " . $t->getMessage();
		exit;
	} catch (Exception $s) {
		echo "ERROR AL AGREGAR DATOS 3 " . $s->getMessage();
		exit;
	}
}
?>